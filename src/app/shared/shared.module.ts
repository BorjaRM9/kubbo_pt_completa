import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { MatSlideToggleModule, MatTableModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    CommonModule,
    MatSlideToggleModule,
    MatTableModule,
    FormsModule
  ],
  exports: [
    HeaderComponent,
    MatSlideToggleModule,
    MatTableModule,
    FormsModule
  ]
})
export class SharedModule { }

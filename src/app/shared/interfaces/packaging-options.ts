export interface PackagingOptions {
    name: string;
    checked: boolean;
}

export interface Stock {
    id: number;
    quantity: number;
    status: string;
    productId: number;
    warehouseId: number;
}

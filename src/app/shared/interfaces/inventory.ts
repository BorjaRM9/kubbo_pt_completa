export interface Inventory {
    warehouseName: string;
    warehouseCity: string;
    warehouseCountry: string;
    stockQuantity: number;
    percentage: number;
}

import { PackagingOptions } from './packaging-options';

export interface Packaging {
    productId: number;
    packagingOptions: PackagingOptions[];
}

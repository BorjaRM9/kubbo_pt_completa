import { Injectable } from '@angular/core';
import { Product } from '../interfaces/product';
import { Stock } from '../interfaces/stock';
import { Warehouse } from '../interfaces/warehouse';
import { PackagingOptions } from '../interfaces/packaging-options';
import { Packaging } from '../interfaces/packagin';
import { WAREHOUSES, PACKAGING, STOCKS, PRODUCTS } from '../mocks';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  products: Product[] = PRODUCTS;
  stocks: Stock[] = STOCKS;
  warehouses: Warehouse[] = WAREHOUSES;
  packaging: Packaging[] = PACKAGING;

  constructor(
  ) { }

  getProducts(): Product[] {
    return this.products;
  }

  getProduct(productId: number): Product {
    return this.products.find(p => p.id === productId);
  }

  getStockByProductId(productId: number): Stock[] {
    return this.stocks.filter(s => s.productId === productId);
  }

  getWarehouse(warehouseId: number) {
    return this.warehouses.find(w => w.id === warehouseId);
  }

  getPackagingOptionsByProductId(productId: number): PackagingOptions[] {
    return this.packaging.find(p => p.productId === productId).packagingOptions;
  }
}

import { PackagingOptions } from '../interfaces/packaging-options';

export const PACKAGING_OPTIONS: PackagingOptions[] = [
    { name: 'Packaging personalizado', checked: false },
    { name: 'Plegable', checked: false },
    { name: 'Envio en el propio contenedor', checked: false },
    { name: 'Flyer promocional', checked: false },
    { name: 'Obsequio promocional', checked: false },
    { name: 'Peligroso', checked: false },
    { name: 'Frágil', checked: false },
];

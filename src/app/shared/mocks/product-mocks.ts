import { Product } from '../interfaces/product';

export const PRODUCTS: Product[] = [
    {id: 1, name: 'Té Matcha', sku: 'SKU 25674852', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: true },
    {id: 2, name: 'Té Matcha premium', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '5,00€', enabled: true },
    {id: 3, name: 'Cuchara Medidora', sku: 'SKU 14875963', barcode: '1234567890', image: '/assets/gris.jpg', price: '4,00€', enabled: false },
    {id: 4, name: 'Té Menta', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: true },
    {id: 5, name: 'Té Naranja', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: true },
    {id: 6, name: 'Té Negro Ecológico', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
    {id: 7, name: 'Té Verde Bio', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: true },
    {id: 8, name: 'product', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
    {id: 9, name: 'product', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
    {id: 10, name: 'product', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
    {id: 11, name: 'product', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
    {id: 12, name: 'product', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
    {id: 13, name: 'product', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
    {id: 14, name: 'product', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
    {id: 15, name: 'product', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
    {id: 16, name: 'product', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
    {id: 17, name: 'product', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
    {id: 18, name: 'product', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
    {id: 19, name: 'product', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
    {id: 20, name: 'product', sku: 'SKU 72056356', barcode: '1234567890', image: '/assets/gris.jpg', price: '3,00€', enabled: false },
];

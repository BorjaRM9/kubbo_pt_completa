import { Warehouse } from '../interfaces/warehouse';

export const WAREHOUSES: Warehouse[] = [
    {id: 1, name: 'BCN 1', city: 'Barcelona', country: 'España' },
    {id: 2, name: 'MAD 1', city: 'Madrid', country: 'España' },
    {id: 3, name: 'VLC 1', city: 'Valencia', country: 'España' },
    {id: 4, name: 'BCN 2', city: 'Barcelona', country: 'España' },
    {id: 5, name: 'MAD 2', city: 'Madrid', country: 'España' },
    {id: 6, name: 'VLC 2', city: 'Valencia', country: 'España' },
    {id: 7, name: 'BCN 3', city: 'Barcelona', country: 'España' },
    {id: 8, name: 'BCN 4', city: 'Barcelona', country: 'España' },
    {id: 9, name: 'LIS 1', city: 'Lisboa', country: 'Portugal' },
    {id: 10, name: 'LIS 2', city: 'Lisboa', country: 'Portugal' },
    {id: 11, name: 'PAR 1', city: 'Paris', country: 'Francia' },
    {id: 12, name: 'PAR 2', city: 'Paris', country: 'Francia' },
    {id: 13, name: 'SEV 1', city: 'Sevilla', country: 'España' },
    {id: 14, name: 'ZAR 1', city: 'Zaragoza', country: 'España' },
];

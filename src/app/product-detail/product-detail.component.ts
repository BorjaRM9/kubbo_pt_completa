import { Component, OnInit } from '@angular/core';
import { Product } from '../shared/interfaces/product';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../shared/services/product.service';


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  product: Product;

  constructor(private route: ActivatedRoute,
              private productService: ProductService) { }

  ngOnInit() {
    this.loadProduct();
  }

  loadProduct() {
    const id: number = +this.route.snapshot.paramMap.get('id');
    this.product = this.productService.getProduct(id);
  }

}

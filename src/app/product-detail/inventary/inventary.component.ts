import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from '../../shared/services/product.service';
import { Stock } from '../../shared/interfaces/stock';
import { Warehouse } from '../../shared/interfaces/warehouse';
import { Inventory } from '../../shared/interfaces/inventory';

interface StockBarWidth {
  width: string;
}

@Component({
  selector: 'app-inventary',
  templateUrl: './inventary.component.html',
  styleUrls: ['./inventary.component.scss']
})
export class InventaryComponent implements OnInit {

  @Input() productId: number;

  stocks: Stock[];
  warehouses: Warehouse[] = [];
  inventory: Inventory[] = [];

  displayedColumns: string[] = ['city', 'country', 'name', 'stock', 'percentage'];

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.stocks = this.productService.getStockByProductId(this.productId);
    const totalProductStock = this.calculateTotalProductStock();

    for (const stock of this.stocks) {
      const warehouse = this.productService.getWarehouse(stock.warehouseId);
      this.warehouses = [...this.warehouses, warehouse];

      const inventoryRow: Inventory = {
        warehouseName: warehouse.name,
        warehouseCity: warehouse.city,
        warehouseCountry: warehouse.country,
        stockQuantity: stock.quantity,
        percentage: this.calculateStockPercentage(stock.quantity, totalProductStock)
      };
      this.inventory = [...this.inventory, inventoryRow];
    }
  }

  calculateTotalProductStock(): number {
    return this.stocks.reduce((acc, stock) => acc + stock.quantity, 0);
  }

  calculateStockPercentage(stockQuantity: number, totalProductStock: number) {
    return stockQuantity / totalProductStock * 100;
  }

  getStockBarWidth(percentage: number): StockBarWidth {
    return { width: percentage + '%' };
  }

}

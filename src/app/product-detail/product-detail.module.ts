import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventaryComponent } from './inventary/inventary.component';
import { ProductInfoComponent } from './product-info/product-info.component';
import { PackagingComponent } from './packaging/packaging.component';
import { ProductDetailComponent } from './product-detail.component';
import { MatCheckboxModule, MatProgressBarModule, MatGridListModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    ProductDetailComponent,
    InventaryComponent,
    ProductInfoComponent,
    PackagingComponent
  ],
  imports: [
    CommonModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatGridListModule,
    SharedModule
  ],
})
export class ProductDetailModule { }

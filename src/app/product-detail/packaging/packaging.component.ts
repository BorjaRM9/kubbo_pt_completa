import { Component, OnInit, Input } from '@angular/core';
import { PackagingOptions } from '../../shared/interfaces/packaging-options';
import { ProductService } from '../../shared/services/product.service';

@Component({
  selector: 'app-packaging',
  templateUrl: './packaging.component.html',
  styleUrls: ['./packaging.component.scss']
})
export class PackagingComponent implements OnInit {

  @Input() productId: number;

  packagingOptions: PackagingOptions[];

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.packagingOptions = this.productService.getPackagingOptionsByProductId(this.productId);
  }


}


import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from '../shared/interfaces/product';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ProductService } from '../shared/services/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  selectedProduct: Product;
  displayedColumns: string[] = ['name', 'sku', 'barcode', 'enabled'];
  dataSource = new MatTableDataSource<Product>(this.productService.getProducts());

  checked = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private router: Router,
              private productService: ProductService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

  navigate(rowId: number) {
    // set timeout to let the animation finish
    setTimeout(() => { this.router.navigateByUrl(`/product-detail/${rowId}`); }, 500);
  }

}

